// InfluxDB exchanges management
//
package unflux

import (
	"fmt"
	"sync"
	"time"
	"strings"
	"github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/ecoc/snmp2influxdb/config"
)

const (
	MAX_MEASURE = 100			// Maximum measure points before send
)

type BaseInfos struct {
	client		client.Client				// InfluxDB connection handler
	verrou		sync.Mutex					// mutual exclusion lock.
	batchPts	client.BatchPoints			// give access to BatchPoints struct
	batchPtsNum	int							// store BatchPoints number 
	conf		*config.Config				// Snmp2InfluxDB configuration (for InfluxCNX).
}

// getNewBatch creates new batch points structure
func getNewBatch( conf *config.Config ) ( client.BatchPoints, error ) {

	if conf.InfluxCnx.Retention != "" {
		return client.NewBatchPoints(  client.BatchPointsConfig{
			Database: conf.InfluxCnx.Base,
			Precision: "ns",
			RetentionPolicy: conf.InfluxCnx.Retention,
		})
	} else {
		return client.NewBatchPoints(  client.BatchPointsConfig{
			Database: conf.InfluxCnx.Base,
			Precision: "ns",
		})
	}
}

// ConnectToInflux creates connexion context with InfluxDB server.
func ConnectToInflux( conf *config.Config ) (*BaseInfos,error) {
	stock := new(BaseInfos)
	stock.conf = conf

	adresse := "http://" + conf.InfluxCnx.Server + ":" + conf.InfluxCnx.Port
	var err error = nil

	if conf.InfluxCnx.User != "" {
		stock.client, err = client.NewHTTPClient(client.HTTPConfig{
										Addr: adresse,
										Username: conf.InfluxCnx.User,
										Password: conf.InfluxCnx.Password,
										Timeout: time.Second * 30,
		})
	} else {
		stock.client, err = client.NewHTTPClient(client.HTTPConfig{
										Addr: adresse,
										Timeout: time.Second * 30,
		})
	}
	if err != nil {
		return nil, err
	}

	stock.batchPts, err = getNewBatch( conf )
	if err != nil {
		return nil, err
	}
	stock.batchPtsNum = 0

	err = stock.CreateDB()
	if err != nil {
		return nil, err
	}

	return stock, nil
}

// CreateDB is used to create a new database.
// If database already exists, no error will return.
func (tf *BaseInfos)CreateDB() error {
	q := client.NewQuery( "CREATE DATABASE \"" + tf.conf.InfluxCnx.Base + "\"", "", "" )
	_, err := tf.client.Query( q )
	if err != nil {
		return err
	}

	return nil
}

// QueryDB will send query to InfluxDB database.
func (tf *BaseInfos)QueryDB( q client.Query ) (*client.Response, error) {
	response, err := tf.client.Query( q )

	if err != nil {
		return nil, err
	} else if response.Error() != nil {
		return nil, response.Error()
	} else {
		return response, nil
	}
}

// Flush send batch points to InfluxDB.
func (tf *BaseInfos)Flush() error {
	err := tf.client.Write(tf.batchPts)
	if err != nil {
		return err
	}
	bp, err := getNewBatch( tf.conf )
	if err != nil {
		return err
	}
	tf.batchPtsNum = 0;
	tf.batchPts = bp

	return nil
}

// AddMeasure adds measurements to batch points and send batch when MAX_MEASURE is reached.
func (tf *BaseInfos)AddMeasure( clt config.Client, mesure config.Mesure ) {
	tf.verrou.Lock()

	tags := map[string]string {
		"host": clt.Host,
		"index": mesure.Index,
	}
	for _, tag := range clt.Tags {
		chns := strings.Split( tag, "=" )
		if len(chns) == 2 {
			tags[chns[0]] = chns[1]
		}
	}
	for _,oid := range mesure.Oids {
		if oid.Type == "tag" {
			switch oid.Value.(type) {
				case string:
					tags[oid.Field] = oid.Value.(string)
				case float32,float64:
					tags[oid.Field] = fmt.Sprintf( "%g", oid.Value )
				default:
					tags[oid.Field] = fmt.Sprintf( "%d", oid.Value )
			}
		}
	}

	fields := make( map[string]interface{}, len(mesure.Oids) )
	for _,oid := range mesure.Oids {
		if oid.Type == "field" {
			fields[oid.Field] = oid.Value
		}
	}

	pt, err := client.NewPoint( mesure.Name, tags, fields, time.Now() )
	if err == nil {
		tf.batchPts.AddPoint( pt )
		tf.batchPtsNum++
	}

	if( tf.batchPtsNum > MAX_MEASURE ) {
		tf.Flush()
	}
	tf.verrou.Unlock()
}
