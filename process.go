package main
// Send and parse SNMP requests
//
import (
	"os"
	"fmt"
	"log"
	"flag"
	"time"
	"strings"
	"net/http"
	"html/template"
	_ "net/http/pprof"

	"bosun.org/snmp/mib"
	"github.com/soniah/gosnmp"
	"gitlab.com/ecoc/snmp2influxdb/config"
	"gitlab.com/ecoc/snmp2influxdb/influx"
)


var Version string
var Build string

var configFile = flag.String( "config", "config.yml", "config file" )
var version = flag.Bool( "version", false, "Prints current version." )
var listenPort = flag.String( "port", "9445", "listen port" )
var debug = flag.Bool( "debug", false, "activer HTTP PPROF." )
var pprofPort = flag.String( "pprof_port", "6060", "HTTP PPROF port." )

var indexTmpl = `
<html>
<head><title>SNMP to InfluxDB agent</title></head>
<body>
	<h1>SNMP to InfluDB</h1>
	<h3>Configuration InfluxDB</h3>
	<ul>
	 {{if .InfluxCnx.Server}} <li>Serveur: {{.InfluxCnx.Server}}:{{.InfluxCnx.Port}} </li> {{end}}
	 {{if .InfluxCnx.User}} <li>User: {{.InfluxCnx.User}} </li> {{end}}
	 {{if .InfluxCnx.Base}} <li>Base: {{.InfluxCnx.Base}} </li> {{end}}
	 {{if .InfluxCnx.Retention}} <li>Retention: {{.InfluxCnx.Retention}} </li> {{end}}
	</ul>
	<h3>Servers being monitored:</h3>
	<ul>
	{{ range .Clients }}
		<li>{{ .Host }}:{{ .Port }}</li>
	{{ end }}
	</ul>
	<h3>Measurements:</h3>
	<ul>
	{{ range .Mesures }}
		<li>{{ .Name }}
			<table>
			  <tr>
			  <th>Name</th><th>Oid</th>
			  </tr>
			{{ range .Oids }}
				<tr><td>{{ .Field }}</td><td>{{ .Oid}}</td></tr>
			{{ end }}
			</table>
		</li>
	{{ end }}
	</ul>
	<h3>Garbage Collection</h3>

</body>
</body>
</html>
`
// EqualButLast reports wether oi and other minus last component represents the same identifier
// Returns true if oi == other minus last oid and last oid
// Warning other must be larger than oi
func EqualButLast( oi string, other string ) (bool,string) {
	index := strings.LastIndexByte( other,  '.' )
	if len(oi) != len(other[1:index]) {
//		fmt.Fprintf( os.Stderr, "len(oi) != (len(other[1:index]) - 1): %d %d\n", len(oi), len(other[1:index]) - 1 )
		return false,""
	}
	if oi != other[1:index] {
//		fmt.Fprintf( os.Stderr, "oi    :%s\nother: %s\n", oi, other[1:index] )
		return false,""
	}

	return true, other[index+1:]
}

func traite( mes *config.Mesure, res *gosnmp.SnmpPacket, oids []string ) bool {
	for i, pdu := range res.Variables {
		eq, idx := EqualButLast( mes.Oids[i].OidN, pdu.Name )
		if eq == false {
			return true
		}
		mes.Index = idx
		oids[i] = pdu.Name

		switch pdu.Type {
			case gosnmp.OctetString:
				bytes := pdu.Value.([]byte)
				switch mes.Oids[i].Conv {
					case "String":
						mes.Oids[i].Value = string(bytes)
					case "Integer":
						var val int64
						if _,err := fmt.Sscanf(string(bytes),"%d", &val ); err == nil {
							mes.Oids[i].Value = val
						} else {
							mes.Oids[i].Value = 0
						}
					case "Real":
						var val float64
						if _,err := fmt.Sscanf(string(bytes),"%f", &val ); err == nil {
							mes.Oids[i].Value = val
						} else {
							mes.Oids[i].Value = 0.0
						}
				}
			default:
				switch mes.Oids[i].Conv {
					case "String":
						mes.Oids[i].Value = fmt.Sprintf( "%s", pdu.Value )
					case "Integer":
						 mes.Oids[i].Value = gosnmp.ToBigInt(pdu.Value).Int64()
					case "Real":
						mes.Oids[i].Value = float64(gosnmp.ToBigInt(pdu.Value).Int64())
				}
		}
	}

	return false
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf( os.Stderr, "usage: snmp2influxdd [option]\n" );
		flag.PrintDefaults()
		os.Exit(2)
	}
	flag.Parse()
	if *version {
		fmt.Fprintf( os.Stdout, "Version: %s\n", Version )
		fmt.Fprintf( os.Stdout, "Build Time: %s\n", Build )
		os.Exit( 0 )
	}
	// Parse config
	conf, err := config.Parse( *configFile )
	if err != nil {
		log.Fatal( "Parse(","config.yml) ", err )
		os.Exit( 1 )
	}
	if ! conf.Validate() {
		os.Exit(1)
	}
	// Parse OIDs
	for pos, mesure := range conf.Mesures {
		for num, oid := range mesure.Oids {
			asn1_oid, err := mib.Lookup( oid.Oid )
			if err != nil {
				log.Fatal( "mib.Lookup(", oid.Oid, "): ", err )
			}
			conf.Mesures[pos].Oids[num].OidN = asn1_oid.String()
		}
	}
	// Open connexion to InfluxDB
	influx, err := unflux.ConnectToInflux( &conf )
	if err != nil {
		log.Fatal( "ConnectToInflux: ", err )
	}
	// Process SNMP request
	for num, _ := range conf.Clients {
		go func( conf config.Config, num int, influx *unflux.BaseInfos ) {
			client := conf.Clients[num]

			version := gosnmp.Version1
			switch client.Version {
				case "1":
					version = gosnmp.Version1
				case "2c":
					version = gosnmp.Version2c
				case "3":
					version = gosnmp.Version3
			}

			g := gosnmp.GoSNMP{
				Target:		client.Host,
				Port:		client.Port,
				Community:	client.Community,
				Version:	version,
				Timeout:	time.Duration(client.Timeout) * time.Second,
				Retries:	3,
				MaxOids:	50,
			}

			err = g.Connect()
			if err != nil {
				log.Fatal( "Connect() err: ", err )
			}
			defer g.Conn.Close()
			// Build oids lists
			oids_queries := make( map[string][]string, len(conf.Mesures) )
			for _, mesure := range conf.Mesures {
				oids_queries[mesure.Name] = make( []string, len(mesure.Oids) )
			}
			for {
				for _, mesure := range conf.Mesures {
					oids := oids_queries[mesure.Name]
					for i, oid := range mesure.Oids {
						oids[i] = oid.OidN
					}
					for {
						result, err := g.GetNext( oids )
						if err != nil {
							fmt.Fprintf( os.Stderr, "GetNext(): %s\n", err )
							break
						}
						if traite( &mesure, result, oids ) {
							break
						}
						influx.AddMeasure( client, mesure )

						g.Timeout = time.Duration(client.Timeout) * time.Second
						result = nil
					}
				}
			}
			time.Sleep( time.Duration(conf.InfluxCnx.Frequency) * time.Second )
		} (conf, num, influx)
	}
	// Start profiler if debug
	if *debug {
		go func() {
			log.Println(http.ListenAndServe("localhost:"+*pprofPort, nil))
		}()
	}
	// Start web server
	var mux = http.NewServeMux()
	var index = template.Must(template.New("index").Parse(indexTmpl))
	mux.HandleFunc( "/",
		func( w http.ResponseWriter, r *http.Request) {
			if err := index.Execute(w, &conf); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		})
	if err := http.ListenAndServe(":"+*listenPort, mux); err != nil {
		log.Fatal( "Failed to start HTTP server: ", err )
	}
}
