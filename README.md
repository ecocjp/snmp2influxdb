# SNMPv2 exporter to InfluxDB.

snmp2influxdb exports SNMPv2 metrics to InfluxDB.

- SNMP metrics may be queried on multiple servers at same time.
- oid may be promoted as tags or fields into InfluxDB
- many oids may be grouped into one measurement.

##Installation:

    Get source from this gitlab repository
    make
    Copy snmp2influxdb executable
    Modify config.yml as needed.

##Options:

    snmp2influxdb --config autre_config.yml [--version --help]


##Configuration:

- Create an YAML file like this:
    
```yaml
influxdb:
    server: InfluxDB server @IP or name
    port:   InfluxDB server port (Ex: 8086)
    user:   Database user
    pwd:    Database password
    base:   Base name

clients:
- host:       'SNMP server @IP or name'
  port:       SNMP server port (Ex: 161)
  version:    'SNMP version 1 or 2c'
  community:  'Community name'
  tags:       [ "tag1=value1", "tag2=value2" ]

- host:       'Another SNMP server'
  port:       SNMP server port (Ex: 161)
  version:    'SNMP version 1 or 2c'
  community:  'Community name'
  tags:       [ "tag1=another_value1", "tag3=....", ... ]

mesures:
- name: measurement_name
  table:
   - field: 'guest'						# Field or tag name
     oid: 'SUN-LDOM-MIB::ldomName'		# OID to be queried
     conv: 'String'						# Conversion from OID type to this one
     type: 'tag'
   - field: 'operstate'					# Another field
     oid: 'SUN-LDOM-MIB::ldomOperState'
     type: 'field'
     conv: 'Integer'
   - field: 'nb-cpu'
     oid: 'SUN-LDOM-MIB::ldomNumVCpu'
     type: 'field'
     conv: 'Integer'
   - field: 'memory'
     oid: 'SUN-LDOM-MIB::ldomMemSize'
     conv: 'Integer'
     type: 'field'
   - field: 'unite'
     oid: 'SUN-LDOM-MIB::ldomMemUnit'
	  ...
	  ...

- name: cpu
  table:
   - field: 'index'
     oid: 'SUN-LDOM-MIB::ldomVcpuLdomIndex'
     conv: 'Integer'
     type: 'tag'
   - field: 'device-id'
     oid: 'SUN-LDOM-MIB::ldomVcpuDeviceID'
     type: 'tag'
     conv: 'String'
     ...
     ...
```

- OID must follow net-snmp requirements. Check correctness with snmptranslate.
- Conversion is made from SNMP simple type to String, Interger or Double
- 'name' is a tag all following OIDs.
- OID are always queried as table. SNMP must always send NEXT oid.
- OID index (last part of OID) is given as index tag automatically.

##To Do:

- Filter on index
- Implements SNMPv3.
