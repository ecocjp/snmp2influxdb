//	Process daemon configuration.
//
package config

import (
	"os"
	"fmt"
	"io/ioutil"
	"gopkg.in/yaml.v2"
)

type Storage struct {
	Server		string	`yaml:"server,omitempty"`
	Port		string	`yaml:"port,omitempty"`
	User		string	`yaml:"user,omitempty"`
	Password	string	`yaml:"pwd,omitempty"`
	Base		string	`yaml:"base,omitempty"`
	Retention	string	`yaml:"retention,omitempty"`
	Frequency	int		`yaml:"freq,omitempty"`
}

type Client struct {
	Host		string		`yaml:"host,omitempty"`
	Port		uint16		`yaml:"port,omitempty"`
	Version		string		`yaml:"version,omitempty"`
	Community	string		`yaml:"community,omitempty"`
	Tags		[]string	`yaml:"tags,omitempty"`
	Timeout		uint		`yaml:"timeout,omitempty"`
}

type Table struct {
   Field	string	`yaml:"field,omitempty"`	// Tag or field name
   Oid		string	`yaml:"oid,omitempty"`		// Oid (ASCII form)
   Type		string	`yaml:"type,omitempty"`		// 'tag' or 'field'
   Conv		string	`yaml:"conv,omitempty"`		// 'Integer', 'String' or 'Real'
   Filter	string	`yaml:"filter,omitempty"`	// Indexes to be kept

   OidN		string								// Oid (Numeric form)
   Value	interface{}
}

type Mesure struct {
	Name	string	`yaml:"name,omitempty"`
	Oids	[]Table	`yaml:"table,omitempty"`

   Index	string
}

type Config struct {
	Mesures		[]Mesure	`yaml:"mesures,omitempty"`
	Clients		[]Client	`yaml:"clients,omitempty"`
	InfluxCnx	Storage		`yaml:"influxdb,omitempty"`
}

// Parse a file into a config instance
func Parse(f string) (config Config, err error) {
	bts, err := ioutil.ReadFile(f)
	if err != nil {
		return
	}
	err = yaml.Unmarshal( bts, &config )

	return
}

// =========== Impression =========

func (m Mesure)validate() bool {
	retour := true

	if m.Name == "" {
		fmt.Fprintf( os.Stderr, "Mesure name undefined.\n" )
		retour = false
	}
	for i,oid := range m.Oids {
		if oid.Field == "" {
			fmt.Fprintf( os.Stderr, "Mesure %s N° %d: field undefined.\n", m.Name, i )
			retour = false
		}
		if oid.Oid == "" {
			fmt.Fprintf( os.Stderr, "Mesure %s N° %d: oid undefined.\n", m.Name, i )
			retour = false
		}
		if oid.Type == "" {
			fmt.Fprintf( os.Stderr, "Mesure %s N° %d: type undefined.\n", m.Name, i )
			retour = false
		} else if oid.Type != "tag" && oid.Type != "field" {
			fmt.Fprintf( os.Stderr, "Mesure %s N° %d: type syntax error.\n", m.Name, i )
			retour = false
		}
		if oid.Conv == "" {
			fmt.Fprintf( os.Stderr, "Mesure %s N° %d: conv undefined.\n", m.Name, i )
			retour = false
		}
	}

	return retour
}

func (c *Client)validate() (*Client,bool,bool) {
	modified := false

	if c.Host == "" {
		fmt.Fprintf( os.Stderr, "Snmp host undefined, default localhost.\n" )
		c.Host = "localhost"
		modified = true
	}
	if c.Port <= 0 || c.Port > 65535 {
		fmt.Fprintf( os.Stderr, "SNMP port invalid, default 161.\n" )
		c.Port = 161
		modified = true
	}
	if c.Version == "" || (c.Version != "1" && c.Version != "2c" && c.Version != "3") {
		fmt.Fprintf( os.Stderr, "SNMP version invalid (1|2c|3).\n" )
		return nil, false, false
	}
	if c.Community == "" {
		fmt.Fprintf( os.Stderr, "SNMP community undefined. default public.\n" )
		c.Community = "public"
		modified = true
	}
	if c.Timeout <= 5 {
		fmt.Fprintf( os.Stderr, "SNMP timeout undefined. default 10 secondes.\n" )
		c.Timeout = 10
		modified = true
	}

	return c, true, modified
}

func (s *Storage)validate() (*Storage,bool,bool) {
	modified := false

	if s.Server == "" {
		fmt.Fprintf( os.Stderr, "InfluxDB server undefined, default localhost.\n" )
		s.Server = "localhost"
	}
	if s.Port == "" {
		fmt.Fprintf( os.Stderr, "InfluxDB server undefined, default 8086.\n" )
		s.Port = "8086"
	}
	if s.Base == "" {
		fmt.Fprintf( os.Stderr, "InfluxDB database undefined, default snmp.\n" )
		s.Base = "snmp"
	}
	if s.Frequency <= 5 {
		s.Frequency = 10
	}

	return s,true,modified
}

func (c Config)Validate() bool {
	if influx,validated,modified := c.InfluxCnx.validate(); !validated {
		return false
	} else {
		if modified { c.InfluxCnx = *influx }
	}
	for i,clt := range c.Clients {
		if client,validated,modified := clt.validate(); ! validated {
			return false
		} else {
			if modified { c.Clients[i] = *client }
		}
	}
	for _,mes := range c.Mesures {
		if ! mes.validate() {
			return false
		}
	}
	return true
}
// =========== Impression =========
func (i Storage)String() string {
	return fmt.Sprintf(
		"influxdb:\n  server: %v\n  port: %v\n  user: %v\n  pwd: %v\n  base: %v\n  retention: %v\n  freq: %d\n",
		i.Server, i.Port, i.User, i.Password, i.Base, i.Retention, i.Frequency )
}

func (c Client)String() string {
	return fmt.Sprintf(
		"\n- host: %v\n  port: %v\n  version: %v\n  community: %v\n  tags: %v\n  timeout: %d\n",
		c.Host, c.Port, c.Version, c.Community, c.Tags, c.Timeout )
}

func (t Table)String() string {
	return fmt.Sprintf( "\n  - field: %v\n    oid: %v\n    type: %v\n    conv: %v\n    oid.num: %v\n",
		t.Field, t.Oid, t.Type, t.Conv, t.OidN )
}

func (m Mesure)String() string {
	return fmt.Sprintf( "\n- name: %v\n  table:\n  %v\n", m.Name, m.Oids )
}

func (c Config)String() string {
	return fmt.Sprintf( "%v\nclients:\n%v\nmesures:\n%v\n", c.InfluxCnx, c.Clients, c.Mesures )
}
